\documentclass[12pt]{article}
%\documentclass[tutorial]{cmpreport}

\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[margin=1.4in]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

%\setlength{\parindent}{2em}
%\setlength{\parskip}{0.25em}
\renewcommand{\baselinestretch}{1.3}

\makeatletter
\input{t1pcr.fd}
\makeatother
\setlength{\footnotesep}{3ex}

\bibliographystyle{apalike}

% Title Page
\title{Graphics 2 - Drone Simulator}
\author{Ben Townsend (100137721)}

\begin{document}
\maketitle

\section{Introduction}

\subsection{Class Structure}
To allow for easy scalability with my project, I created a base \verb|Object| class. This class specifies basic geometry to draw, and stores basic vectors such as position, rotation and scale. 

I then extend this into a \verb|ModelObject| class, which takes in a model name as a string. This means that I can easily create a subclass for a new object, specify the model name, and the superclass will perform all the object loading and rendering.

\section{Models and Textures}

\subsubsection{Drone}

I created the drone model myself using 3DS Max, originally with the blades and lights as part of the same model. Later, I split the blades and lights into their own models. This allows me to load them in separately, so that the blades can be moved independently from the drone and the lights can be switched on and off.

I applied a basic grey carbon fibre texture to the drone itself, whereas the blades had a darker version of the same texture.

\begin{figure}
	\centering
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{images/drone1.png}
		\caption{Initial drone.}
		\label{fig:sub1}
	\end{subfigure}%
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{images/drone2.png}
		\caption{Final drone.}
		\label{fig:sub2}
	\end{subfigure}
	\caption{Comparison between initial and final versions of the drone.}
	\label{fig:test}
\end{figure}

\subsubsection{Terrain}
I created the terrain also using 3DS Max, it consists of a plane which I used the “Edit Mesh” tool on, with the aim of creating a natural looking world. I decided to keep the terrain relatively simple with a low poly count in order to perform signed area of a tetrahedron tests for collision detection.

With the terrain, the model itself was the playable area, with additional mirrored models either side, to give the appearance of a longer world.

\begin{figure}
	\centering
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{images/terrain1.png}
		\caption{Initial terrain.}
		\label{fig:sub1}
	\end{subfigure}%
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{images/terrain2.png}
		\caption{Final terrain.}
		\label{fig:sub2}
	\end{subfigure}
	\caption{Comparison between initial and final versions of the terrain.}
	\label{fig:test}
\end{figure}

\subsubsection{Objects}
The only object used in the final version is the tree, which I created using 3DS Max. I chose a relatively simple cone shape for the tree, to benefit performance and collision detection down the line.

\section{Drone Flight}

\subsubsection{Controls}
The drone can be moved forward, backward, left and right using ‘W’, ‘S’, ‘A’ and ‘D’ appropriately, or the respective arrow keys. A combination of forward and sideways motion rotates the drone in that direction, alternatively ‘Q’ and ‘E’ can be used to rotate left and right on the spot.

The drone can also move up using ‘SPACE’ and can move down using ‘SHIFT’ or ‘CTRL’.

\subsubsection{Physics}
Each element of the drone’s motion takes acceleration and velocity into account. Holding a directional key will change the target velocity of that element to a set amount, which the drone will then increase acceleration to attempt to meet. This target velocity is then set back to 0 when the key is released, and the drone will attempt to decrease acceleration to stop. This creates a self-balancing effect, where the drone will attempt to reset its total velocity to 0 when no keys are being held.

The drone also performs rotations around the Z and X axes in response to acceleration in the X and Z directions, this creates the effect of the drone tilting towards the direction it is travelling.

All the movement-based calculations feature a multiplication by deltaT. This is the average time each frame takes to draw, based upon a sample of the previous 1000 frames, stored in an array. I found that taking the time from only one frame would create jarring movement, and a minimum of 1000 was required to create smooth motion. 

\section{Camera/Views}
The simulator uses the following 4 different cameras or views:
\begin{itemize}
	\item Third person camera, positioned behind and above the drone.
	\item First person camera, from the drone’s point of view.
	\item Ground camera, positioned at the drone’s starting location and looks at the drone.
	\item Above camera, positioned at the centre of the map and looks at the drone.
\end{itemize}
These cameras can be switched between using ‘C’ or selected specifically using ‘1’, ‘2’, ‘3’ or ‘4’.

\section{AI Drone}

\subsection{Follower Drone}
My first idea for an AI drone was one which followed the player. My thinking was that further down the line, this could be used as an enemy drone or target that would follow and attempt to attack the player.

I used an \verb|atan2| function to calculate the angle between the AI drone and the player. If the angle were greater than zero, the drone would rotate to the right, and vice versa. If the distance between the AI drone and the player was greater than 10, it would increase forward acceleration. If the height of the AI drone was higher than the player, it would descend, and vice versa.

\subsection{Way-point Drone}
My second idea was to have a drone which navigated to certain way-points on the map in a loop. But unfortunately was unable to finish this in time. This would also use an \verb|atan2| function, to calculate the rotations needed for the drone to face the way-point.

\section{Collision Detection}
For the collision detection I used 3 different approaches for 3 different scenarios.

\subsubsection{Drone-Drone Collisions}
For the drone and AI drone collisions with each other, I used spherical bounding boxes. For this, I calculated and tested the euclidean distance between the drones, and if it was less than the sum of the longest length from each drone, it performed a collision response.

The response makes each drone bounce away from each other, by calculating the vectors between them and increasing each drone's velocity in that direction.

Even though spherical tests are not the most precise, it worked well for the fast moving drones, where speed is needed more than accuracy.

\subsubsection{Drone-Terrain Collisions}
For the drone and terrain collisions, I used a signed volume of a tetrahedron test.\cite{O'Rourke:1994:CGC:528314} The bottom three points are the triangle from the terrain's geometry, and the top point is the test point on the drone.

As the terrain is just a simple plane, the triangles making up the geometry are all defined the same way. After testing, I found that if the volume returned was negative, the test point was outside of the plane, and if the volume was positive, the test point had crossed the plane.

In order to work out which triangles to test the points against, I calculated the distance between each triangle and the drone, and then tested the closest one.

Once I was able to work out whether or not a collision was occurring, I was able to create a response. For this, I calculated the normal vector of the colliding triangle, normalised it, then increase the drone's velocity in that direction. This creates a bounce effect specific to the collided geometry.

Eventually, I ended up using 4 test points, one for each corner of the drone. This created a realistic and accurate bounding box, where the drone would never clip into the terrain.
\subsubsection{Drone-Object Collisions}
For the drone and object collisions, I tested the octrees of the object against test points on the drone. 

For this, I created a recursive method which takes a test point, and tests if the point is within the min/max values. If true, it calls the same method on the children. The method will return true if the point is within a cube, and is on the bottom level of the octree (a leaf).

For the collision response, I calculated the vector between the drone and the centre of the object, on collision, the drone will bounce back in the direction of the vector.

\section{Lighting and Shaders}

\subsubsection{Material Properties}
For each object in the scene, I configured the material properties to accurately reflect how they would appear in real life.

For example, the ice had maximum shininess, and very high specularity, to make the surface appear to reflect a lot of light. The snow had less specularity and shininess, but more diffuse, to make it look like the snow was scattering the light from the sun. The drone and trees had more of a matte texture, with less specular and diffuse.

\subsubsection{Light Properties}
I set the light position to very high in the sky, to avoid the drone being able to fly past it. The colour during day mode was a warm white attempting to mimic the colour of the sun, with a brightness level. 

The colour during night mode was a cool blue, with a lower brightness, which is intended to look like moonlight. The light position was also slightly different, to give an impression that time has passed.

\begin{figure}
	\centering
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{images/day.png}
		\caption{Day mode.}
		\label{fig:sub1}
	\end{subfigure}%
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{images/night.png}
		\caption{Night mode.}
		\label{fig:sub2}
	\end{subfigure}
	\caption{Comparison between the different lighting modes.}
	\label{fig:test}
\end{figure}

\subsubsection{Fog}
I also implemented fog, through a method of mixing the fog colour with the pixel colour based on the depth of the fragment. 

I used the same colour as the sky, to create a smooth transition to the far clipping plane. I experimented with different levels of fog, and found that fog starting at 100 depth and ending at 400 depth looked nice and did not restrict the view too much.

\subsubsection{Sky}
I also implemented a night sky texture for the night mode, using a textured sphere with a unique shader, which is unaffected by the light or fog in the scene.

\subsubsection{Spotlight}
One thing I tried to implement was a spotlight for the drone, but unfortunately, I was unable to finish this in time.

I thought this would pair quite well with the night mode, as it could light up shadows on the terrain.

\section{Particle Effects}
One thing I also implemented was smoke particle effects. When colliding with something, the drone will emit a burst of smoke particles.

After taking too much damage, the drone will emit constant smoke, signifying that one or two more hits will destroy the drone.

\bibliography{reportbib}

\end{document}          
