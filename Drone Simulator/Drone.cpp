#include "Drone.h"

OffLight::OffLight() : ModelObject("models/lightsOff.obj")
{
	Material_Ambient[0] = 1.0f; Material_Ambient[1] = 1.0f; Material_Ambient[2] = 1.0f;
	Material_Diffuse[0] = 1.0f; Material_Diffuse[1] = 1.0f; Material_Diffuse[2] = 1.0f;
	Material_Specular[0] = 1.0f; Material_Specular[1] = 1.0f; Material_Specular[2] = 1.0f;
	//Material_Shininess = 100;
}

void OffLight::preditionalDisplay() {
	glUniform1i(glGetUniformLocation(textureShader.handle(), "fogEnabled"), false);
}

void OffLight::additionalDisplay() {
	glUniform1i(glGetUniformLocation(textureShader.handle(), "fogEnabled"), true);
}

OffLight::~OffLight()
{
}

RedLight::RedLight() : ModelObject("models/redLights.obj")
{
	Material_Ambient[0] = 1.0f; Material_Ambient[1] = 1.0f; Material_Ambient[2] = 1.0f;
	Material_Diffuse[0] = 1.0f; Material_Diffuse[1] = 1.0f; Material_Diffuse[2] = 1.0f;
	Material_Specular[0] = 1.0f; Material_Specular[1] = 1.0f; Material_Specular[2] = 1.0f;
	//Material_Shininess = 100;
}

void RedLight::preditionalDisplay() {
	glUniform1i(glGetUniformLocation(textureShader.handle(), "fogEnabled"), false);
}

void RedLight::additionalDisplay() {
	glUniform1i(glGetUniformLocation(textureShader.handle(), "fogEnabled"), true);
}

RedLight::~RedLight()
{
}

GreenLight::GreenLight() : ModelObject("models/greenLights.obj")
{
	Material_Ambient[0] = 1.0f; Material_Ambient[1] = 1.0f; Material_Ambient[2] = 1.0f;
	Material_Diffuse[0] = 1.0f; Material_Diffuse[1] = 1.0f; Material_Diffuse[2] = 1.0f;
	Material_Specular[0] = 1.0f; Material_Specular[1] = 1.0f; Material_Specular[2] = 1.0f;
	//Material_Shininess = 100;
}

void GreenLight::preditionalDisplay() {
	glUniform1i(glGetUniformLocation(textureShader.handle(), "fogEnabled"), false);
}

void GreenLight::additionalDisplay() {
	glUniform1i(glGetUniformLocation(textureShader.handle(), "fogEnabled"), true);
}

//void GreenLight::update()
//{
//}
//
//void GreenLight::display()
//{
//}

GreenLight::~GreenLight()
{
}

Blade::Blade() : ModelObject("models/blade.obj")
{
	//Material_Ambient[0] = 0.1f; Material_Ambient[1] = 0.1f; Material_Ambient[2] = 0.1f;
	//Material_Diffuse[0] = 0.8f; Material_Diffuse[1] = 0.8f; Material_Diffuse[2] = 0.5f;
	Material_Specular[0] = 0.1f; Material_Specular[1] = 0.1f; Material_Specular[2] = 0.1f;
	Material_Shininess = 50;
}

void Blade::update()
{
	if (spin > 360)
		spin = 0;
	spin += 5*deltaT*factor;
}

void Blade::display() {
	if (invisible)
		return;

	glUseProgram(textureShader.handle());  // use the shader
	
	ModelMatrix = glm::mat4(1.0f);

	ModelMatrix = glm::translate(ModelMatrix, position);	//simple translate

	ModelMatrix = glm::rotate(ModelMatrix, rotation.y, glm::vec3(0, 1, 0)); //rotation about y axis
	ModelMatrix = glm::rotate(ModelMatrix, rotation.x, glm::vec3(1, 0, 0)); //rotation about x axis
	ModelMatrix = glm::rotate(ModelMatrix, rotation.z, glm::vec3(0, 0, 1)); //rotation about z axis

	ModelMatrix = glm::translate(ModelMatrix, displacement);	//simple translate 

	ModelMatrix = glm::rotate(ModelMatrix, -spin, glm::vec3(0, 1, 0)); //rotation about y axis

	ModelViewMatrix = viewMatrix * ModelMatrix;

	glUniform4fv(glGetUniformLocation(textureShader.handle(), "material_ambient"), 1, Material_Ambient);
	glUniform4fv(glGetUniformLocation(textureShader.handle(), "material_diffuse"), 1, Material_Diffuse);
	glUniform4fv(glGetUniformLocation(textureShader.handle(), "material_specular"), 1, Material_Specular);
	glUniform1f(glGetUniformLocation(textureShader.handle(), "material_shininess"), Material_Shininess);

	glm::mat3 normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
	glUniformMatrix3fv(glGetUniformLocation(textureShader.handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);

	//Pass the uniform for the modelview matrix - in this case just "r"
	glUniformMatrix4fv(glGetUniformLocation(textureShader.handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
	model.drawElementsUsingVBO(&textureShader);

	glFlush();

	additionalDisplay();


}

Blade::~Blade()
{
}

Drone::Drone() : ModelObject("models/drone.obj")
{ 
	//Material_Ambient[0] = 0.1f; Material_Ambient[1] = 0.1f; Material_Ambient[2] = 0.1f;
	//Material_Diffuse[0] = 0.8f; Material_Diffuse[1] = 0.8f; Material_Diffuse[2] = 0.8f;
	Material_Specular[0] = 0.1f; Material_Specular[1] = 0.1f; Material_Specular[2] = 0.1f;
	Material_Shininess = 50;
}

void Drone::additionalInit() {
	particleSystem = new ParticleSystem();

	for (int i = 0; i<4; i++)
		blades.push_back(new Blade());

	for (Blade* b : blades)
		b->createGeometry();

	blades.at(0)->displacement = glm::vec3(-0.65, 0.18, -0.65);
	blades.at(1)->displacement = glm::vec3(0.65, 0.18, -0.65);
	blades.at(2)->displacement = glm::vec3(0.65, 0.18, 0.65);
	blades.at(3)->displacement = glm::vec3(-0.65, 0.18, 0.65);

	greenLight = new GreenLight();
	greenLight->createGeometry();
	redLight = new RedLight();
	redLight->createGeometry();
	offLight = new OffLight();
	offLight->createGeometry();

	for (Blade* b : blades) {
		b->position = position;
		b->rotation = rotation;
	}
}

void Drone::update() {
	float accelerationIncrement = 0.000004f*deltaT;
	float decayAmount = 0.000003f*deltaT;

	float rotationalAccelerationIncrement = 0.00002f*deltaT;
	float rotationalDecayAmount = 0.000015f*deltaT;

	//Movement
	if (targetVelocity.x > velocity.x) {
		acceleration.x += accelerationIncrement;
	}
	else if (targetVelocity.x < velocity.x) {
		acceleration.x -= accelerationIncrement;
	}
	//Decay
	if (acceleration.x > 0) {
		acceleration.x -= decayAmount;
	}
	else if (acceleration.x < 0) {
		acceleration.x += decayAmount;
	}
	//Update
	velocity.x += acceleration.x*deltaT;
	//position.x += velocity.x;
	float rotationRadians = PI * (rotation.y / 180);
	position.z -= (velocity.x * sin(rotationRadians));
	position.x += (velocity.x * cos(rotationRadians));
	//Rotation
	//rotation.z = -velocity.x * 2000;

	//Movement
	if (targetVelocity.y > velocity.y) {
		acceleration.y += accelerationIncrement;
	}
	else if (targetVelocity.y < velocity.y) {
		acceleration.y -= accelerationIncrement;
	}
	//Decay
	if (acceleration.y > 0) {
		acceleration.y -= decayAmount;
	}
	else if (acceleration.y < 0) {
		acceleration.y += decayAmount;
	}
	//Update
	velocity.y += acceleration.y*deltaT;
	position.y += velocity.y;
	//Spinning blades
	/*blades.at(0)->factor = 1.0f + (velocity.y * 10 * deltaT);
	blades.at(1)->factor = 1.0f + (velocity.y * 10 * deltaT);
	blades.at(2)->factor = 1.0f + (velocity.y * 10 * deltaT);
	blades.at(3)->factor = 1.0f + (velocity.y * 10 * deltaT);*/

	//Movement
	if (targetVelocity.z > velocity.z) {
		acceleration.z += accelerationIncrement;
	}
	else if (targetVelocity.z < velocity.z) {
		acceleration.z -= accelerationIncrement;
	}
	//Decay
	if (acceleration.z > 0) {
		acceleration.z -= decayAmount;
	}
	else if (acceleration.z < 0) {
		acceleration.z += decayAmount;
	}
	//Update
	velocity.z += acceleration.z*deltaT;
	//position.z += velocity.z;
	rotationRadians = PI * ((rotation.y - 90) / 180);
	position.z -= (velocity.z * sin(rotationRadians));
	position.x += (velocity.z * cos(rotationRadians));
	//Rotation
	rotation.x = (velocity.z * 500)/deltaT;

	///Rotation
	//Movement
	if (rotationalTargetVelocity > rotationalVelocity) {
		rotationalAcceleration += rotationalAccelerationIncrement;
	}
	else if (rotationalTargetVelocity < rotationalVelocity) {
		rotationalAcceleration -= rotationalAccelerationIncrement;
	}
	//Decay
	if (rotationalAcceleration > 0) {
		rotationalAcceleration -= rotationalDecayAmount;
	}
	else if (rotationalAcceleration < 0) {
		rotationalAcceleration += rotationalDecayAmount;
	}
	//Update
	rotationalVelocity += rotationalAcceleration;
	rotation.y += rotationalVelocity * deltaT;
	//Rotation
	rotation.z = ((rotationalVelocity * 100) + (-velocity.x * 500))/deltaT;


	if (absoluteVelocity.x > 0) {
		absoluteVelocity.x -= decayAmount * 50.0f;
	}
	else if (absoluteVelocity.x < 0) {
		absoluteVelocity.x += decayAmount * 50.0f;
	}

	if (absoluteVelocity.y > 0) {
		absoluteVelocity.y -= decayAmount * 50.0f;
	}
	else if (absoluteVelocity.y < 0) {
		absoluteVelocity.y = 0;
		//absoluteVelocity.y += decayAmount * 50.0f;
	}

	if (absoluteVelocity.z > 0) {
		absoluteVelocity.z -= decayAmount * 50.0f;
	}
	else if (absoluteVelocity.z < 0) {
		absoluteVelocity.z += decayAmount * 50.0f;
	}

	position += absoluteVelocity;

	for (Blade* b : blades) {
		b->position = position;
		b->rotation = rotation;
		if (online)
			b->update();
	}

	if(debug)
		std::cout << "X=" << position.x << "Y=" << position.y << "Z=" << position.z << std::endl;
	
	glm::vec3 temp = position;
	temp.y += 0.25;
	particleSystem->update(temp, rotation, health < 2);

	if (rotation.y >= 360)
		rotation.y = 0;
	else if (rotation.y < 0)
		rotation.y = 360;

	redLight->position = position;
	redLight->rotation = rotation;

	greenLight->position = position;
	greenLight->rotation = rotation;

	offLight->position = position;
	offLight->rotation = rotation;

	if (lightCoolDown < 0) {
		if (lightState < 3)
			lightState++;
		else
			lightState = 0;

		lightCoolDown = 500.0f;
	}
	else {
		lightCoolDown -= 1.0f*deltaT;
	}

	if (invulnerable > 0)
		invulnerable -= 1.0f*deltaT;
}

void Drone::preditionalDisplay() {
	for (Blade* b : blades)
		b->display();

	if (lightState == 0)
		redLight->display();
	else if(lightState == 1)
		offLight->display();
	else if (lightState == 2)
		greenLight->display();
	else if (lightState == 3)
		offLight->display();

	particleSystem->display();
}

void Drone::keyboard() {
	float targetVelocityVariable = 0.05f*deltaT;

	if (keys['I']) {
		online = !online;
		keys['I'] = false;
	}

	if (keys['H']) {
		health = 5;
		keys['H'] = false;
	}

	if (online) {
		if ((keys['A'] || keys[VK_LEFT]) && !(keys['W'] || keys[VK_UP]) && !(keys['S'] || keys[VK_DOWN]))
		{
			targetVelocity.x = -targetVelocityVariable;
		}
		else if ((keys['D'] || keys[VK_RIGHT]) && !(keys['W'] || keys[VK_UP]) && !(keys['S'] || keys[VK_DOWN])) {
			targetVelocity.x = targetVelocityVariable;
		}
		else {
			targetVelocity.x = 0;
		}

		if (keys[VK_SPACE])
		{
			targetVelocity.y = targetVelocityVariable;
		}
		else if (keys[VK_SHIFT] || keys[VK_CONTROL]) {
			targetVelocity.y = -targetVelocityVariable;
		}
		else {
			targetVelocity.y = 0;
		}

		if ((keys['W'] || keys[VK_UP]) && !(keys['D'] || keys[VK_RIGHT]) && !(keys['A'] || keys[VK_LEFT]))
		{
			targetVelocity.z = -targetVelocityVariable;
		}
		else if (keys['S'] || keys[VK_DOWN] && !(keys['D'] || keys[VK_RIGHT]) && !(keys['A'] || keys[VK_LEFT])) {
			targetVelocity.z = targetVelocityVariable;
		}
		else {
			targetVelocity.z = 0;
		}

		if (keys['Q']) {
			rotationalTargetVelocity = 0.02f*deltaT;
		}
		else if (keys['E']) {
			rotationalTargetVelocity = -0.02f*deltaT;
		}
		else if ((keys['W'] || keys[VK_UP]) && (keys['A'] || keys[VK_LEFT])) {
			targetVelocity.x = -targetVelocityVariable * 0.75f;
			targetVelocity.z = -targetVelocityVariable * 0.75f;
			rotationalTargetVelocity = 0.01f*deltaT;
		}
		else if ((keys['W'] || keys[VK_UP]) && (keys['D'] || keys[VK_RIGHT])) {
			targetVelocity.x = targetVelocityVariable * 0.75f;
			targetVelocity.z = -targetVelocityVariable * 0.75f;
			rotationalTargetVelocity = -0.01f*deltaT;
		}
		else if ((keys['S'] || keys[VK_DOWN]) && (keys['A'] || keys[VK_LEFT])) {
			targetVelocity.x = -targetVelocityVariable * 0.75f;
			targetVelocity.z = targetVelocityVariable * 0.75f;
			rotationalTargetVelocity = -0.01f*deltaT;
		}
		else if ((keys['S'] || keys[VK_DOWN]) && (keys['D'] || keys[VK_RIGHT])) {
			targetVelocity.x = targetVelocityVariable * 0.75f;
			targetVelocity.z = targetVelocityVariable * 0.75f;
			rotationalTargetVelocity = 0.01f*deltaT;
		}
		else {
			rotationalTargetVelocity = 0;
		}
	}
	else
		targetVelocity.y = -targetVelocityVariable*4;
}

void Drone::respawn(glm::vec3 position)
{
	this->position = position;
	velocity = glm::vec3(0, 0, 0);
	acceleration = glm::vec3(0, 0, 0);
	targetVelocity = glm::vec3(0, 0, 0);
	rotationalVelocity = 0;
	rotationalAcceleration = 0;
	rotationalTargetVelocity = 0;
	absoluteVelocity = glm::vec3(0, 0, 0);
	health = 5;
}

Drone::~Drone()
{
}

AIDrone::AIDrone()
{
}

void AIDrone::keyboard()
{

	float targetVelocityVariable = 0.02f*deltaT;

	float XZdistance = ((position.x - playerPos.x) * (position.x - playerPos.x) + (position.z - playerPos.z) * (position.z - playerPos.z));
	float Ydistance = position.y - playerPos.y;

	float angle = atan2(playerPos.z - position.z, position.x - playerPos.x);
	angle *= 180 / PI;
	angle += 90;

	if (angle < 0)
		angle += 360;

	if (angle > 360)
		angle -= 360;

	float difference = rotation.y - angle;

	if (difference < -180)
		difference += 360;

	if (difference > 180)
		difference -= 360;

	//std::cout << difference << std::endl;

	if (difference < -5)
	{
		//std::cout << "AI: I should turn left" << std::endl;
		rotationalTargetVelocity = 0.02f*deltaT*((difference / -180) + 0.1);
	}
	else if (difference > 5)
	{
		//std::cout << "AI: I should turn right" << std::endl;
		rotationalTargetVelocity = -0.02f*deltaT*((difference / 180) + 0.1);
	}
	else
		rotationalTargetVelocity = 0;

	if (XZdistance > 225 && (0 < difference < 45 || 0 > difference > -45)) { //Move to at least 15 metres away
		targetVelocity.z = -targetVelocityVariable;
	}
	else if (XZdistance < 36) { //Keep at least 6 metres away
		targetVelocity.z = targetVelocityVariable;
	}
	else
		targetVelocity.z = 0;

	if (Ydistance > 0.5) {
		targetVelocity.y = -targetVelocityVariable*0.5f * ((Ydistance / 10) + 0.1);
	}
	else if (Ydistance < -0.5) {
		targetVelocity.y = targetVelocityVariable * 0.5f * ((Ydistance / -10) + 0.1);
	}
	else
		targetVelocity.y = 0;

}

AIDrone::~AIDrone()
{
}
