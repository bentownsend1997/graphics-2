#pragma once

#include "Object.h"
#include <list>

class Particle : public Object
{
public:
	float lifespan, maxLifespan;
	glm::vec3 velocity;

	Particle(glm::vec3 position, glm::vec3 rotation, float lifespan);
	void setColours();
	void update();
	~Particle();
};

class ParticleSystem
{
public:
	list<Particle*> particles;
	int maxParticles;

	glm::vec3 position = glm::vec3(0, 0, 0);
	glm::vec3 rotation = glm::vec3(0, 0, 0);

	float timer = 0;
	int burst = 0;

	ParticleSystem();
	void display();
	void update(glm::vec3 position, glm::vec3 rotation, bool constant);
	~ParticleSystem();
};