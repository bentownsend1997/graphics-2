//OPENGL 3.2 EXAMPLE

//NB: DEPRECATED COMMANDS WILL NOT FUNCTION.
//SEE OPENGL QUICK REFERENCE FOR DETAILS

const float PI = 3.14159265358f;

#include "Global.h"
#include "Object.h"
#include "Drone.h"
#include "Enviroment.h"

#include "3DStruct\threeDModel.h"
#include "Obj\OBJLoader.h"
#include "Octree\Octree.h"

#include <iostream>
#include <vector>
#include <math.h>     //sin and cos included in this header file.

////Global bois////
float deltaT = 2.0f;
Shader basicShader;
Shader textureShader;
glm::mat4 viewMatrix;
bool keys[256];			// Array Used in the keyboard method.

int	mouse_x = 0, mouse_y = 0;
bool LeftPressed = false;

bool debug = false;
bool boxes = false;
bool particlesEnabled = false;

glm::vec3 playerPos;
std::vector<AIDrone*> enemyDrones;
///////////////////

__int64 prevTime = 0;
__int64 pastDeltaT[1024];
int startingUp = 0;

bool paused = true;
bool startup = true;

enum CameraType { THIRD, DRONE, STATIC, ABOVE };
CameraType camera = THIRD;

int screenWidth=1280, screenHeight=720;

std::vector<ModelObject*> worldObjects;
Drone* drone;
Ground* ground;
NightSky* nightSky;
Title* title;

float enemyDroneCooldown = 0;
std::vector<glm::vec3> droneSpawns;
int maxEnemyDrones = 1;

Line* line;

glm::vec3 startPos = glm::vec3(51, 0.95, 98);

//Light Properties
float Light_Ambient_And_Diffuse[4] = { 0.8f, 0.8f, 0.8f, 1.0f };
//float Light_Ambient_And_Diffuse[4] = { 0.2f, 0.2f, 0.3f, 1.0f };
float Light_Specular[4] = { 1.0f,1.0f,1.0f,1.0f };
float LightPos[4] = { -1000.0f, 2000.0f, 0.0f, 1.0f };

float Fog_Color[4] = { 0.8,  0.9, 1.0, 0.0 };

//OPENGL FUNCTION PROTOTYPES
void init();				//called in winmain when the program starts.
void reshape(int width, int height);
void display();				//called in winmain to draw everything to the screen
bool solveOctree(Octree* octree, glm::vec3 point, glm::mat4 ModelMatrix);
glm::vec3 intersectingModel(ModelObject* object, glm::vec3 d);
void update();
void keyboard();     //called to check key presses
void calculateDeltaT();

/*************    START OF OPENGL FUNCTIONS   ****************/

glm::mat4 ProjectionMatrix; // matrix for the orthographic projection

void init()
{

	//glClearColor(0.8, 0.9, 1.0, 0.0);						//sets the clear colour to yellow
	glClearColor(Fog_Color[0], Fog_Color[1], Fog_Color[2], Fog_Color[3]);						//sets the clear colour to yellow
					//glClear(GL_COLOR_BUFFER_BIT) in the display function//will clear the buffer to this colour.

	glEnable(GL_DEPTH_TEST);

	if(!textureShader.load("BasicView", "glslfiles/textures.vert", "glslfiles/textures.frag"))
	{
		std::cout << "failed to load shader" << std::endl;
	}

	if (!basicShader.load("BasicView", "glslfiles/basic.vert", "glslfiles/basic.frag"))
	{
		std::cout << "failed to load shader" << std::endl;
	}

	//glEnable(GL_CULL_FACE);
	//glCullFace(GL_BACK);

	drone = new Drone();
	drone->position = startPos;
	drone->createGeometry();

	droneSpawns.push_back(glm::vec3(0, 20, -20));
	droneSpawns.push_back(glm::vec3(0, 30, 0));
	droneSpawns.push_back(glm::vec3(0, 40, -50));
	droneSpawns.push_back(glm::vec3(0, 50, -30));

	ground = new Ground();
	ground->createGeometry();

	nightSky = new NightSky();
	nightSky->createGeometry();
	nightSky->invisible = true;

	title = new Title();
	title->createGeometry();
	title->position = glm::vec3(0, 20, 0);

	worldObjects.push_back(new Water());

	/*Cabin* cabin = new Cabin();
	cabin->position = glm::vec3(60, 0, 50);
	worldObjects.push_back(cabin);*/

	worldObjects.push_back(new Water());

	vector<glm::vec3> treeLocations;
	treeLocations.push_back(glm::vec3(30, 5.5, 0));
	treeLocations.push_back(glm::vec3(52, 5.5, 72));
	treeLocations.push_back(glm::vec3(59, 1.8, -92));
	treeLocations.push_back(glm::vec3(-44, 19.7, -124));
	treeLocations.push_back(glm::vec3(-48, 4.4, -25));
	treeLocations.push_back(glm::vec3(-12.5, 13, 163.5));
	treeLocations.push_back(glm::vec3(32, 4, 54));
	treeLocations.push_back(glm::vec3(38, 8, 32));
	treeLocations.push_back(glm::vec3(50, 10, -26));
	treeLocations.push_back(glm::vec3(-51, 13, -54));
	treeLocations.push_back(glm::vec3(-35, 7, -82));
	treeLocations.push_back(glm::vec3(-63.7, 10, 5));
	treeLocations.push_back(glm::vec3(-31, 6, 138));
	treeLocations.push_back(glm::vec3(9, 8, 148));

	ModelObject* tree = new Tree();
	for (glm::vec3 vec : treeLocations) {
		tree = new Tree();
		tree->position = vec;
		tree->scale = tree->scale * float((rand() % 200)/100.0f);
		//tree->rotation.y = rand() % 360;
		worldObjects.push_back(tree);
	}

	Ground* extraGround = new Ground();
	extraGround->position = glm::vec3(0, 0, 400);
	extraGround->scale.x = -1;
	extraGround->rotation.y = 180;
	worldObjects.push_back(extraGround);

	/*extraGround = new Ground();
	extraGround->position = glm::vec3(0, 0, 800);
	extraGround->rotation.y = 0;
	worldObjects.push_back(extraGround);*/

	extraGround = new Ground();
	extraGround->position = glm::vec3(0, 0, -400);
	extraGround->scale.x = -1;
	extraGround->rotation.y = 180;
	worldObjects.push_back(extraGround);

	/*extraGround = new Ground();
	extraGround->position = glm::vec3(0, 0, -800);
	extraGround->rotation.y = 0;
	worldObjects.push_back(extraGround);*/

	for(ModelObject* o : worldObjects)
		o->createGeometry();

	line = new Line();
	line->createGeometry();

}

void reshape(int width, int height)		// Resize the OpenGL window
{
	screenWidth = width; 
	screenHeight = height;

	glViewport(0,0,width,height);						// set Viewport dimensions

	ProjectionMatrix = glm::perspective(60.0f, (GLfloat)screenWidth/(GLfloat)screenHeight, 1.0f, 1000.0f);
}

void display()									
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	glUseProgram(textureShader.handle());  // use the shader

	//Lighting and material information

	glUniform4fv(glGetUniformLocation(textureShader.handle(), "LightPos"), 1, LightPos);
	glUniform4fv(glGetUniformLocation(textureShader.handle(), "light_ambient"), 1, Light_Ambient_And_Diffuse);
	glUniform4fv(glGetUniformLocation(textureShader.handle(), "light_diffuse"), 1, Light_Ambient_And_Diffuse);
	glUniform4fv(glGetUniformLocation(textureShader.handle(), "light_specular"), 1, Light_Specular);

	glUniform4fv(glGetUniformLocation(textureShader.handle(), "fogColor"), 1, Fog_Color);

	float spotPosition[4] = { drone->position.x, drone->position.y, drone->position.z, 1.0f };
	glUniform4fv(glGetUniformLocation(textureShader.handle(), "spotPosition"), 1, spotPosition);

	float spotRotation[4] = { 0.0f, 0.0f, -1.0f, 1.0f };
	glUniform4fv(glGetUniformLocation(textureShader.handle(), "spotRotation"), 1, spotRotation);

	glUniform1f(glGetUniformLocation(textureShader.handle(), "spotCutoff"), glm::cos(glm::radians(12.5f)));

	viewMatrix = glm::mat4(1.0f);

	if (startup) {
		viewMatrix = glm::translate(viewMatrix, -glm::vec3(0, 20, 27.5));
	}
	else if (camera == STATIC) {
		glm::vec3 cameraPos = startPos;
		cameraPos.y += 4;
		cameraPos.z += 4;
		viewMatrix = glm::lookAt(cameraPos, drone->position, glm::vec3(0, 1, 0));
	}
	else if (camera == THIRD) {
		viewMatrix = glm::translate(viewMatrix, -glm::vec3(0, 1.5, 4));
		//viewMatrix = glm::rotate(viewMatrix, -drone->rotation.x, glm::vec3(1, 0, 0)); //rotation about x axis
		viewMatrix = glm::rotate(viewMatrix, -drone->rotation.y, glm::vec3(0, 1, 0)); //rotation about y 
		//viewMatrix = glm::rotate(viewMatrix, -drone->rotation.z, glm::vec3(0, 0, 1)); //rotation about z axis
		viewMatrix = glm::translate(viewMatrix, -drone->position);	//simple translate
		
	}
	else if (camera == ABOVE) {
		viewMatrix = glm::lookAt(glm::vec3(glm::vec3(0, drone->position.y + 20, 0)), drone->position, glm::vec3(0, 1, 0));
	}
	else if (camera == DRONE) {
		//viewMatrix = glm::rotate(viewMatrix, -drone->rotation.x, glm::vec3(1, 0, 0)); //rotation about x axis
		viewMatrix = glm::rotate(viewMatrix, -drone->rotation.y, glm::vec3(0, 1, 0)); //rotation about y axis
		//viewMatrix = glm::rotate(viewMatrix, -drone->rotation.z, glm::vec3(0, 0, 1)); //rotation about z axis
		viewMatrix = glm::translate(viewMatrix, -drone->position);	//simple translate
	}

	if (camera == DRONE || camera == THIRD)
		nightSky->position = drone->position;
	else
		nightSky->position = glm::vec3(0, 0, 0);

	glUniformMatrix4fv(glGetUniformLocation(textureShader.handle(), "ViewMatrix"), 1, GL_FALSE, &viewMatrix[0][0]);

	GLuint matLocation = glGetUniformLocation(textureShader.handle(), "ProjectionMatrix");
	glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);

	glUseProgram(basicShader.handle());  // use the shader

	glUniformMatrix4fv(glGetUniformLocation(textureShader.handle(), "ViewMatrix"), 1, GL_FALSE, &viewMatrix[0][0]);

	matLocation = glGetUniformLocation(textureShader.handle(), "ProjectionMatrix");
	glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);

	drone->display();
	ground->display();
	nightSky->display();

	for (AIDrone* ai : enemyDrones)
		ai->display();

	for (ModelObject* o : worldObjects)
		o->display();

	if (startup)
		title->display();

	line->display();

	glBindVertexArray(0); //unbind the vertex array object
	
	glUseProgram(0); //turn off the current shader
}

bool solveOctree(Octree* octree, glm::vec3 point, glm::mat4 ModelMatrix) {
	glm::vec3 min, max;
	min.x = octree->minX;
	max.x = octree->maxX;
	min.y = octree->minY;
	max.y = octree->maxY;
	min.z = octree->minZ;
	max.z = octree->maxZ;

	min = glm::vec3(ModelMatrix * glm::vec4(min, 1.0f));
	max = glm::vec3(ModelMatrix * glm::vec4(max, 1.0f));

	/*std::cout << min.x << " " << min.y << " " << min.z;
	std::cout << "    " << point.x << " " << point.y << " " << point.z;
	std::cout << "    " << max.x << " " << max.y << " " << max.z << std::endl;*/

	if (point.x > min.x && point.x < max.x && point.y > min.y && point.y < max.y && point.z > min.z && point.z < max.z) {

		//std::cout << octree->Level << std::endl;

		if (octree->Level == 0){
			return true;
		}

		for (Octree* child : octree->children) {
			if(child != NULL)
				return solveOctree(child,point, ModelMatrix);
		}

	}

	return false;
}

glm::vec3 intersectingModel(ModelObject* object, glm::vec3 d) {
	ThreeDModel* model = &object->model;
	//std::cout << model->vertexPositionList[6] << std::endl;

	glm::vec3 a;
	glm::vec3 b;
	glm::vec3 c;

	glm::vec3 result = glm::vec3(0,0,0);

	//For each triangle
	float lowestDistance[4] = { 1000000,1000000,1000000,1000000 };
	float closestArea[4];
	glm::vec3 closestCentrePoint[4];
	glm::vec3 closestNormal;

	for (int count = 0; count < model->numberOfTriangles * 3 * 3; count += 9)
	{
		//	Vector3d bob = theVerts[count];
		a.x = model->vertexPositionList[count];
		a.y = model->vertexPositionList[count + 1];
		a.z = model->vertexPositionList[count + 2];

		b.x = model->vertexPositionList[count + 3];
		b.y = model->vertexPositionList[count + 4];
		b.z = model->vertexPositionList[count + 5];

		c.x = model->vertexPositionList[count + 6];
		c.y = model->vertexPositionList[count + 7];
		c.z = model->vertexPositionList[count + 8];

		glm::vec3 centrePoint = (a + b + c) / 3.0f;

		glm::vec3 normal = glm::normalize(glm::cross(b - a, c - a));

		//std::cout << normal.x << " " << normal.y << " " << normal.z << std::endl;

		float distance = ((centrePoint.x - d.x) * (centrePoint.x - d.x) + (centrePoint.y - d.y) * (centrePoint.y- d.y) + (centrePoint.z - d.z) * (centrePoint.z - d.z));

		//break;

		float area = -(a.z - d.z)*(b.y - d.y)*(c.x - d.x) + (a.y - d.y)*(b.z - d.z)*(c.x - d.x)
			+ (a.z - d.z)*(b.x - d.x)*(c.y - d.y) - (a.x - d.x)*(b.z - d.z)*(c.y - d.y)
			- (a.y - d.y)*(b.x - d.x)*(c.z - d.z) + (a.x - d.x)*(b.y - d.y)*(c.z - d.z);

		if (abs(distance) < abs(lowestDistance[0])) {
			closestArea[0] = area;
			lowestDistance[0] = distance;
			closestCentrePoint[0] = centrePoint;
			closestNormal = normal;
			//std::cout << "New smallest of " << area << std::endl;
		}
		//else if (abs(distance) < abs(lowestDistance[1])) {
		//	closestArea[1] = area;
		//	lowestDistance[1] = distance;
		//	closestCentrePoint[1] = centrePoint;
		//	//std::cout << "New smallest of " << area << std::endl;
		//}
		//else if (abs(distance) < abs(lowestDistance[2])) {
		//	closestArea[2] = area;
		//	lowestDistance[2] = distance;
		//	closestCentrePoint[2] = centrePoint;
		//	//std::cout << "New smallest of " << area << std::endl;
		//}
		//else if (abs(distance) < abs(lowestDistance[3])) {
		//	closestArea[3] = area;
		//	lowestDistance[3] = distance;
		//	closestCentrePoint[3] = centrePoint;
		//	//std::cout << "New smallest of " << area << std::endl;
		//}
	}

	//std::cout << a.x << " " << a.y << " " << a.z << "    " << b.x << " " << b.y << " " << b.z << "    " << c.x << " " << c.y << " " << c.z << std::endl;
	//std::cout << area << std::endl;

	if (closestArea[0] >= 0) {
		result = closestNormal;
	}
	/*else if (closestArea[1] >= 0) {
		resultVector = &(d - closestCentrePoint[1]);
	}*/
	/*else if (closestArea[2] >= 0) {
		resultVector = &(d - closestCentrePoint[2]);
	}
	else if (closestArea[3] >= 0) {
		resultVector = &(d - closestCentrePoint[3]);
	}*/

	return result;//  || closestArea[1] >= 0|| closestArea[2] >= 0 || closestArea[3] >= 0;
}

void update() {
	drone->update();
	ground->update();
	nightSky->update();

	glm::vec3 testPoints[4];

	for (int i = 0; i < 4; i++) {
		testPoints[i] = glm::vec3(drone->blades.at(i)->ModelMatrix * glm::vec4(0.0f,0.0f,0.0f,1.0f));
	}

	//std::cout << drone->rotation.y << std::endl;

	for (glm::vec3 testPoint : testPoints) {
		//Player terrain collisions
		glm::vec3 result = intersectingModel(ground, testPoint);
		if (result != glm::vec3(0, 0, 0)) {
			drone->absoluteVelocity = glm::vec3(result * 0.025f*deltaT);
			drone->velocity = glm::vec3(0, 0, 0);
			if (particlesEnabled)
				drone->particleSystem->burst = 3;
			if (drone->invulnerable <= 0) {
				drone->health--;
				drone->invulnerable = 100;
			}
			break;
		}
		//Player ground collisions
		if (testPoint.y <= 0) {
			drone->velocity = glm::vec3(0, 1, 0) * 0.025f*deltaT;
			if (particlesEnabled)
				drone->particleSystem->burst = 3;
			if (drone->invulnerable <= 0) {
				drone->health--;
				drone->invulnerable = 100;
			}
			break;
		}
		//Player wall collisions
		if (testPoint.z <= -200) {
			drone->absoluteVelocity = glm::vec3(0, 0, 1)* 0.025f*deltaT;
			drone->velocity = glm::vec3(0, 0, 0);
			/*if (particlesEnabled)
				drone->particleSystem->burst = 3;
			if (drone->invulnerable <= 0) {
				drone->health--;
				drone->invulnerable = 100;
			}*/
			break;
		}
		else if (testPoint.z >= 200) {
			drone->absoluteVelocity = glm::vec3(0, 0, -1) * 0.025f*deltaT;
			drone->velocity = glm::vec3(0, 0, 0);
			/*if (particlesEnabled)
				drone->particleSystem->burst = 3;
			if (drone->invulnerable <= 0) {
				drone->health--;
				drone->invulnerable = 100;
			}*/
			break;
		}
		//Player ceiling collisions
		while (testPoint.y >= 140) {
			drone->velocity = glm::vec3(0, -1, 0) * 0.025f*deltaT, 1.0f;
			/*if (particlesEnabled)
				drone->particleSystem->burst = 3;
			if (drone->invulnerable <= 0) {
				drone->health--;
				drone->invulnerable = 100;
			}*/
			break;
		}
	}

	if (drone->health == 0)
		drone->respawn(startPos);

	for (AIDrone* ai : enemyDrones) {
		ai->update();

		glm::vec3 testPoints[4];

		for (int i = 0; i < 4; i++) {
			testPoints[i] = glm::vec3(ai->blades.at(i)->ModelMatrix * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
		}

		for (glm::vec3 testPoint : testPoints) {
			//AI terrain collisions
			glm::vec3 result = intersectingModel(ground, testPoint);
			if (result != glm::vec3(0, 0, 0)) {
				ai->absoluteVelocity = result * 0.025f*deltaT;
				ai->velocity = glm::vec3(0, 0, 0);
				if (particlesEnabled)
					ai->particleSystem->burst = 3;
				if (ai->invulnerable <= 0) {
					ai->health--;
					ai->invulnerable = 100;
				}
				break;
			}
			//AI ground collisions
			if (testPoint.y <= 0) {
				ai->velocity = glm::vec3(0, 1, 0) * 0.025f*deltaT;
				if (particlesEnabled)
					ai->particleSystem->burst = 3;
				if (ai->invulnerable <= 0) {
					ai->health--;
					ai->invulnerable = 100;
				}				
				break;
			}
			//AI wall collisions
			if (testPoint.z <= -200) {
				ai->absoluteVelocity = glm::vec3(0, 0, 1)* 0.025f*deltaT;
				ai->velocity = glm::vec3(0, 0, 0);
				/*if (particlesEnabled)
					ai->particleSystem->burst = 3;
				if (ai->invulnerable <= 0) {
					ai->health--;
					ai->invulnerable = 100;
				}	*/			
				break;
			}
			else if (testPoint.z >= 200) {
				ai->absoluteVelocity = glm::vec3(0, 0, -1) * 0.025f*deltaT;
				ai->velocity = glm::vec3(0, 0, 0);
				/*if (particlesEnabled)
					ai->particleSystem->burst = 3;
				ai->health--;*/
				break;
			}
			//AI ceiling collisions
			while (testPoint.y >= 140) {
				ai->velocity = glm::vec3(0, -1, 0) * 0.025f*deltaT;
				/*if (particlesEnabled)
					ai->particleSystem->burst = 3;
				if (ai->invulnerable <= 0) {
					ai->health--;
					ai->invulnerable = 100;
				}	*/			
				break;
			}

		}
		if (ai->health == 0) {
			int location = rand() % 3;
			ai->respawn(droneSpawns.at(location));
		}

		//Player - ai collsions

		if (glm::distance(drone->position, ai->position) < 2) {
			ai->velocity = glm::vec3(0,0,0);
			drone->velocity = glm::vec3(0,0,0);

			glm::vec3 droneDeflect = glm::normalize(drone->position - ai->position);
			glm::vec3 aiDeflect = glm::normalize(ai->position - drone->position);

			drone->absoluteVelocity = droneDeflect * 0.015f*deltaT;
			ai->absoluteVelocity = aiDeflect * 0.015f*deltaT;
		}
	}

	for (ModelObject* o : worldObjects) {
		o->update();

		Octree* octree = o->model.octree;

		//Player object collisions
		glm::vec3 middle = o->position + glm::vec3(0,((octree->maxY-octree->minY)/2.0f)*o->scale.y,0);
		glm::vec3 temp = glm::normalize(drone->position - middle);
		if (solveOctree(octree, drone->position, o->ModelMatrix)) {
			drone->absoluteVelocity = temp * 0.025f*deltaT;
			drone->velocity = glm::vec3(0, 0, 0);
			if(particlesEnabled)
				drone->particleSystem->burst = 3;
			if (drone->invulnerable <= 0) {
				drone->health--;
				drone->invulnerable = 100;
			}			
			break;
		}

		//AI object collisions
		for (AIDrone* ai : enemyDrones) {
			//AI object collisions
			glm::vec3 middle = o->position + glm::vec3(0, ((octree->maxY - octree->minY) / 2.0f)*o->scale.y, 0);
			glm::vec3 temp = glm::normalize(ai->position - middle);
			if (solveOctree(octree, ai->position, o->ModelMatrix)) {
				ai->absoluteVelocity = temp * 0.025f*deltaT;
				ai->velocity = glm::vec3(0, 0, 0);
				if (particlesEnabled)
					ai->particleSystem->burst = 3;
				if (ai->invulnerable <= 0) {
					ai->health--;
					ai->invulnerable = 100;
				}
				break;
			}
		}
	}

	playerPos = drone->position;

	if(enemyDroneCooldown > 0)
		enemyDroneCooldown -= 1.0f*deltaT;

	if (enemyDrones.size() < maxEnemyDrones && enemyDroneCooldown <= 0) {
		int location = rand() % 3;

		AIDrone* AIdrone = new AIDrone();
		AIdrone->position = droneSpawns.at(location);
		AIdrone->createGeometry();
		enemyDrones.push_back(AIdrone);		

		enemyDroneCooldown = 1000.0f;
	}

	line->point1 = drone->position;
	line->point2 = glm::vec3(0, 0, 0);
}

void keyboard()
{
	if (keys['C']) {
		if (camera == THIRD) {
			camera = DRONE;
			drone->invisible = true;
			std::cout << "Camera switched to DRONE" << std::endl;
		}
		else if (camera == DRONE) {
			camera = STATIC;
			drone->invisible = false;
			std::cout << "Camera switched to STATIC" << std::endl;
		}
		else if (camera == STATIC) {
			camera = ABOVE;
			drone->invisible = false;
			std::cout << "Camera switched to ABOVE" << std::endl;
		}
		else if (camera == ABOVE) {
			camera = THIRD;
			drone->invisible = false;
			std::cout << "Camera switched to THIRD" << std::endl;
		}
		keys['C'] = false;
	}

	if (keys['1'] && camera != THIRD) {
		camera = THIRD;
		drone->invisible = false;
		std::cout << "Camera switched to THIRD" << std::endl;
		keys['1'] = false;
	}
	else if (keys['2'] && camera != DRONE) {
		camera = DRONE;
		drone->invisible = true;
		std::cout << "Camera switched to DRONE" << std::endl;
		keys['2'] = false;
	}
	else if (keys['3'] && camera != STATIC) {
		camera = STATIC;
		drone->invisible = false;
		std::cout << "Camera switched to STATIC" << std::endl;
		keys['3'] = false;
	}
	else if (keys['4'] && camera != ABOVE) {
		camera = ABOVE;
		drone->invisible = false;
		std::cout << "Camera switched to ABOVE" << std::endl;
		keys['4'] = false;
	}

	if (keys[VK_DELETE]) {
		boxes = !boxes;
		keys[VK_DELETE] = false;
	}

	if (keys[VK_BACK]) {
		debug = !debug;
		keys[VK_BACK] = false;
	}

	if (keys[VK_RETURN]) {
		paused = !paused;
		startup = false;
		keys[VK_RETURN] = false;
	}

	if (keys['P']) {
		particlesEnabled = !particlesEnabled;
		keys['P'] = false;
	}

	if (keys['F']) {
		drone->position = startPos;
		keys['F'] = false;
	}

	if (keys['N']) {
		Fog_Color[0] = 0.8; Fog_Color[1] = 0.9; Fog_Color[2] = 1.0;
		glClearColor(0.8,0.9,1.0,0.0);
		Light_Ambient_And_Diffuse[0] = 0.8; Light_Ambient_And_Diffuse[1] = 0.8; Light_Ambient_And_Diffuse[2] = 0.8;
		LightPos[0] = -1000.0f; LightPos[1] = 2000.f; LightPos[2] = 0.0f;
		nightSky->invisible = true;
		keys['N'] = false;
	}

	if (keys['M']) {
		Fog_Color[0] = 0.05; Fog_Color[1] = 0.05; Fog_Color[2] = 0.1;
		glClearColor(0.039, 0.039, 0.129,0.0);
		Light_Ambient_And_Diffuse[0] = 0.2; Light_Ambient_And_Diffuse[1] = 0.2; Light_Ambient_And_Diffuse[2] = 0.3;
		LightPos[0] = 1000.0f; LightPos[1] = 2000.f; LightPos[2] = 0.0f;
		nightSky->invisible = false;
		keys['M'] = false;
	}

	/*if (LeftPressed && enemyDrones.size() > 0) {
		enemyDrones.pop_back();
		LeftPressed = false;
	}*/

	drone->keyboard();

	for (AIDrone* ai : enemyDrones)
		ai->keyboard();

	for (Object* o : worldObjects)
		o->keyboard();
}

void calculateDeltaT() {
	LARGE_INTEGER t;
	QueryPerformanceCounter(&t);
	__int64 currentTime = t.QuadPart;

	__int64 ticksElapsed = currentTime - prevTime;

	//deltaT = float(ticksElapsed) * 0.0003f;

	float deltaTFrame = float(ticksElapsed) * 0.0003f;

	__int64 temp;
	for (int i = 1023; i > 0; i--) {
		pastDeltaT[i] = pastDeltaT[i-1];
	}

	pastDeltaT[0] = deltaTFrame;

	deltaT = 0;
	for (__int64 time : pastDeltaT)
		deltaT += time;

	deltaT /= 1024;

	//std::cout << deltaT << std::endl;

	//deltaT = 1.0f;
	prevTime = currentTime;
}


/**************** END OPENGL FUNCTIONS *************************/

//WIN32 functions
LRESULT	CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);	// Declaration For WndProc
void KillGLWindow();									// releases and destroys the window
bool CreateGLWindow(char* title, int width, int height); //creates the window
int WINAPI WinMain(	HINSTANCE, HINSTANCE, LPSTR, int);  // Win32 main function

//win32 global variabless
HDC			hDC=NULL;		// Private GDI Device Context
HGLRC		hRC=NULL;		// Permanent Rendering Context
HWND		hWnd=NULL;		// Holds Our Window Handle
HINSTANCE	hInstance;		// Holds The Instance Of The Application


/******************* WIN32 FUNCTIONS ***************************/
int WINAPI WinMain(	HINSTANCE	hInstance,			// Instance
					HINSTANCE	hPrevInstance,		// Previous Instance
					LPSTR		lpCmdLine,			// Command Line Parameters
					int			nCmdShow)			// Window Show State
{
	AllocConsole();
	FILE* stream;
	freopen_s(&stream, "CONOUT$", "w", stdout);
	
	MSG		msg;									// Windows Message Structure
	bool	done=false;								// Bool Variable To Exit Loop

	// Create Our OpenGL Window
	if (!CreateGLWindow("Drone Simulator",screenWidth,screenHeight))
	{
		return 0;									// Quit If Window Was Not Created
	}

	while(!done)									// Loop That Runs While done=FALSE
	{
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	// Is There A Message Waiting?
		{
			if (msg.message==WM_QUIT)				// Have We Received A Quit Message?
			{
				done=true;							// If So done=TRUE
			}
			else									// If Not, Deal With Window Messages
			{
				TranslateMessage(&msg);				// Translate The Message
				DispatchMessage(&msg);				// Dispatch The Message
			}
		}
		else										// If There Are No Messages
		{
			if (keys[VK_ESCAPE] && startup) {
				done = true;
				keys[VK_ESCAPE] = false;
			}

			if (keys[VK_ESCAPE] && !startup) {
				startup = true;
				paused = true;
				keys[VK_ESCAPE] = false;
			}

			if (startingUp < 2048)
				startingUp++;
			else {
				keyboard();
				display();					// Draw The Scene

				if (!paused) 
					update();

				SwapBuffers(hDC);				// Swap Buffers (Double Buffering)
			}

			calculateDeltaT();
		}
	}

	
	// Shutdown
	KillGLWindow();									// Kill The Window
	return (int)(msg.wParam);						// Exit The Program
}

//WIN32 Processes function - useful for responding to user inputs or other events.
LRESULT CALLBACK WndProc(	HWND	hWnd,			// Handle For This Window
							UINT	uMsg,			// Message For This Window
							WPARAM	wParam,			// Additional Message Information
							LPARAM	lParam)			// Additional Message Information
{
	switch (uMsg)									// Check For Windows Messages
	{
		case WM_CLOSE:								// Did We Receive A Close Message?
		{
			PostQuitMessage(0);						// Send A Quit Message
			return 0;								// Jump Back
		}
		case WM_SIZE:								// Resize The OpenGL Window
		{
			reshape(LOWORD(lParam),HIWORD(lParam));  // LoWord=Width, HiWord=Height
			return 0;								// Jump Back
		}
		case WM_KEYDOWN:							// Is A Key Being Held Down?
		{
			keys[wParam] = true;					// If So, Mark It As TRUE
			return 0;								// Jump Back
		}
		case WM_KEYUP:								// Has A Key Been Released?
		{
			keys[wParam] = false;					// If So, Mark It As FALSE
			return 0;								// Jump Back
		}
		case WM_LBUTTONDOWN:
			{
	            mouse_x = LOWORD(lParam);          
				mouse_y = screenHeight - HIWORD(lParam);
				LeftPressed = true;
			}
		break;

		case WM_LBUTTONUP:
			{
	            LeftPressed = false;
			}
		break;

		case WM_MOUSEMOVE:
			{
	            mouse_x = LOWORD(lParam);          
				mouse_y = screenHeight  - HIWORD(lParam);
			}
		break;
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

void KillGLWindow()								// Properly Kill The Window
{
	if (hRC)											// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL,NULL))					// Are We Able To Release The DC And RC Contexts?
		{
			MessageBox(NULL,"Release Of DC And RC Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(hRC))						// Are We Able To Delete The RC?
		{
			MessageBox(NULL,"Release Rendering Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}
		hRC=NULL;										// Set RC To NULL
	}

	if (hDC && !ReleaseDC(hWnd,hDC))					// Are We Able To Release The DC
	{
		MessageBox(NULL,"Release Device Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hDC=NULL;										// Set DC To NULL
	}

	if (hWnd && !DestroyWindow(hWnd))					// Are We Able To Destroy The Window?
	{
		MessageBox(NULL,"Could Not Release hWnd.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hWnd=NULL;										// Set hWnd To NULL
	}

	if (!UnregisterClass("OpenGL",hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL,"Could Not Unregister Class.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hInstance=NULL;									// Set hInstance To NULL
	}
}

/*	This Code Creates Our OpenGL Window.  Parameters Are:					*
 *	title			- Title To Appear At The Top Of The Window				*
 *	width			- Width Of The GL Window Or Fullscreen Mode				*
 *	height			- Height Of The GL Window Or Fullscreen Mode			*/
 
bool CreateGLWindow(char* title, int width, int height)
{
	GLuint		PixelFormat;			// Holds The Results After Searching For A Match
	WNDCLASS	wc;						// Windows Class Structure
	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style
	RECT		WindowRect;				// Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left=(long)0;			// Set Left Value To 0
	WindowRect.right=(long)width;		// Set Right Value To Requested Width
	WindowRect.top=(long)0;				// Set Top Value To 0
	WindowRect.bottom=(long)height;		// Set Bottom Value To Requested Height

	hInstance			= GetModuleHandle(NULL);				// Grab An Instance For Our Window
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraw On Size, And Own DC For Window.
	wc.lpfnWndProc		= (WNDPROC) WndProc;					// WndProc Handles Messages
	wc.cbClsExtra		= 0;									// No Extra Window Data
	wc.cbWndExtra		= 0;									// No Extra Window Data
	wc.hInstance		= hInstance;							// Set The Instance
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);			// Load The Default Icon
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground	= NULL;									// No Background Required For GL
	wc.lpszMenuName		= NULL;									// We Don't Want A Menu
	wc.lpszClassName	= "OpenGL";								// Set The Class Name

	if (!RegisterClass(&wc))									// Attempt To Register The Window Class
	{
		MessageBox(NULL,"Failed To Register The Window Class.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;											// Return FALSE
	}
	
	dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
	dwStyle=WS_OVERLAPPEDWINDOW;							// Windows Style
	
	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);		// Adjust Window To True Requested Size

	// Create The Window
	if (!(hWnd=CreateWindowEx(	dwExStyle,							// Extended Style For The Window
								"OpenGL",							// Class Name
								title,								// Window Title
								dwStyle |							// Defined Window Style
								WS_CLIPSIBLINGS |					// Required Window Style
								WS_CLIPCHILDREN,					// Required Window Style
								0, 0,								// Window Position
								WindowRect.right-WindowRect.left,	// Calculate Window Width
								WindowRect.bottom-WindowRect.top,	// Calculate Window Height
								NULL,								// No Parent Window
								NULL,								// No Menu
								hInstance,							// Instance
								NULL)))								// Dont Pass Anything To WM_CREATE
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Window Creation Error.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		24,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		24,											// 24Bit Z-Buffer (Depth Buffer)  
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};
	
	if (!(hDC=GetDC(hWnd)))							// Did We Get A Device Context?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Device Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!(PixelFormat=ChoosePixelFormat(hDC,&pfd)))	// Did Windows Find A Matching Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Find A Suitable PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if(!SetPixelFormat(hDC,PixelFormat,&pfd))		// Are We Able To Set The Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Set The PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	HGLRC tempContext = wglCreateContext(hDC);
	wglMakeCurrent(hDC, tempContext);
	
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		std::cout << " GLEW ERROR" << std::endl;
		
	}
	
	int attribs[] =
	{
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
		WGL_CONTEXT_MINOR_VERSION_ARB, 2,
		WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
		0
	};
	
    if(wglewIsSupported("WGL_ARB_create_context") == 1)
    {
		hRC = wglCreateContextAttribsARB(hDC,0, attribs);
		wglMakeCurrent(NULL,NULL);
		wglDeleteContext(tempContext);
		wglMakeCurrent(hDC, hRC);
	}
	else
	{	//It's not possible to make a GL 3.x context. Use the old style context (GL 2.1 and before)
		hRC = tempContext;
		std::cout << " not possible to make context "<< std::endl;
	}

	//Checking GL version
	const GLubyte *GLVersionString = glGetString(GL_VERSION);

	std::cout << "OpenGL version: " << GLVersionString << std::endl;

	//We can check the version in OpenGL 
	int OpenGLVersion[2];
	glGetIntegerv(GL_MAJOR_VERSION, &OpenGLVersion[0]);
	glGetIntegerv(GL_MINOR_VERSION, &OpenGLVersion[1]);

	std::cout << "OpenGL Version: " << OpenGLVersion[0] << " " << OpenGLVersion[1] << std::endl;

	ShowWindow(hWnd,SW_SHOW);						// Show The Window
	SetForegroundWindow(hWnd);						// Slightly Higher Priority
	SetFocus(hWnd);									// Sets Keyboard Focus To The Window
	
	init();

	reshape(width, height);     					// Set Up Our Perspective GL Screen
	
	return true;									// Success
}



