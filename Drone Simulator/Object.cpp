#include "Object.h"

Line::Line()
{

}

void Line::createGeometry()
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(basicShader.handle());  // use the shader

	// First simple object
	float dim = 0.5;
	verts[0] = point1.x;   verts[1] = point1.y;  verts[2] = point1.z;
	verts[3] = point2.x;   verts[4] = point2.y;  verts[5] = point2.z;

	cols[0] = 1.0;   cols[1] = 1.0;  cols[2] = 1.0;
	cols[3] = 0.0;   cols[4] = 0.0;  cols[5] = 1.0;

	// VAO allocation
	glGenVertexArrays(1, &m_vaoID);

	// First VAO setup
	glBindVertexArray(m_vaoID);

	glGenBuffers(2, m_vboID);

	glBindBuffer(GL_ARRAY_BUFFER, m_vboID[0]);
	//initialises data storage of vertex buffer object
	glBufferData(GL_ARRAY_BUFFER, numOfVerts * 3 * sizeof(GLfloat), verts, GL_STATIC_DRAW);

	GLint vertexLocation = glGetAttribLocation(basicShader.handle(), "in_Position");
	glVertexAttribPointer(vertexLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(vertexLocation);

	glBindBuffer(GL_ARRAY_BUFFER, m_vboID[1]);
	glBufferData(GL_ARRAY_BUFFER, numOfVerts * 3 * sizeof(GLfloat), cols, GL_STATIC_DRAW);


	GLint colorLocation = glGetAttribLocation(basicShader.handle(), "in_Color");
	glVertexAttribPointer(colorLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(colorLocation);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glEnableVertexAttribArray(0);

	glBindVertexArray(0);
}

void Line::display()
{
	glUseProgram(basicShader.handle());  // use the shader

	//draw objects
	glBindVertexArray(m_vaoID);		// select VAO
	
	glDrawArrays(GL_LINES, 0, numOfVerts); //draw some geometry

	// Done
	glBindBuffer(GL_ARRAY_BUFFER, 0);  //unbind the buffer
	
	glBindVertexArray(0); //unbind the vertex array object
	
	glUseProgram(0); //turn off the current shader
}

void Line::update()
{
}

Line::~Line()
{
}

Object::Object()
{

}

Object::Object(float xSize, float ySize, float zSize)
{
	size = glm::vec3(xSize, ySize, zSize);
}

void Object::setColours() {
	cols[0] = 0.0f;   cols[1] = 0.0f;  cols[2] = 0.0f;		//0
	cols[3] = 0.0f;   cols[4] = 1.0f;  cols[5] = 0.0f;		//1
	cols[6] = 0.0f;   cols[7] = 0.0f;  cols[8] = 1.0f;		//2
	cols[9] = 1.0f;   cols[10] = 1.0f;  cols[11] = 1.0f;	//3

	cols[12] = 1.0f;   cols[13] = 0.0f;  cols[14] = 0.0f;	//4
	cols[15] = 0.0f;   cols[16] = 1.0f;  cols[17] = 0.0f;	//5
	cols[18] = 0.0f;   cols[19] = 0.0f;  cols[20] = 1.0f;	//6
	cols[21] = 1.0f;   cols[22] = 1.0f;  cols[23] = 0.0f;	//7
}

void Object::createGeometry()
{
	glUseProgram(basicShader.handle());  // use the shader

	verts[0] = -size.x / 2;   verts[1] = -size.y / 2;  verts[2] = -size.z / 2;	//0
	verts[3] = -size.x / 2;   verts[4] = size.y / 2;  verts[5] = -size.z / 2;		//1
	verts[6] = size.x / 2;   verts[7] = size.y / 2;  verts[8] = -size.z / 2;		//2
	verts[9] = size.x / 2;   verts[10] = -size.y / 2;  verts[11] = -size.z / 2;	//3

	verts[12] = -size.x / 2;   verts[13] = -size.y / 2;  verts[14] = size.z / 2;	//4
	verts[15] = -size.x / 2;   verts[16] = size.y / 2;  verts[17] = size.z / 2;	//5
	verts[18] = size.x / 2;   verts[19] = size.y / 2;  verts[20] = size.z / 2;	//6
	verts[21] = size.x / 2;   verts[22] = -size.y / 2;  verts[23] = size.z / 2;	//7

	this->setColours();

	tris[0] = 0; tris[1] = 1; tris[2] = 2;
	tris[3] = 0; tris[4] = 2; tris[5] = 3;
	tris[6] = 4; tris[7] = 6; tris[8] = 5;
	tris[9] = 4; tris[10] = 7; tris[11] = 6;
	tris[12] = 1; tris[13] = 5; tris[14] = 6;
	tris[15] = 1; tris[16] = 6; tris[17] = 2;
	tris[18] = 0; tris[19] = 7; tris[20] = 4;
	tris[21] = 0; tris[22] = 3; tris[23] = 7;
	tris[24] = 0; tris[25] = 5; tris[26] = 1;
	tris[27] = 0; tris[28] = 4; tris[29] = 5;
	tris[30] = 3; tris[31] = 2; tris[32] = 7;
	tris[33] = 2; tris[34] = 6; tris[35] = 7;

	// VAO allocation
	glGenVertexArrays(1, &m_vaoID);

	// First VAO setup
	glBindVertexArray(m_vaoID);

	glGenBuffers(2, m_vboID);

	glBindBuffer(GL_ARRAY_BUFFER, m_vboID[0]);
	//initialises data storage of vertex buffer object
	glBufferData(GL_ARRAY_BUFFER, numOfVerts * 3 * sizeof(GLfloat), verts, GL_STATIC_DRAW);
	GLint vertexLocation = glGetAttribLocation(basicShader.handle(), "in_Position");
	glVertexAttribPointer(vertexLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(vertexLocation);


	glBindBuffer(GL_ARRAY_BUFFER, m_vboID[1]);
	glBufferData(GL_ARRAY_BUFFER, numOfVerts * 3 * sizeof(GLfloat), cols, GL_STATIC_DRAW);
	GLint colorLocation = glGetAttribLocation(basicShader.handle(), "in_Color");
	glVertexAttribPointer(colorLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(colorLocation);


	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numOfTris * 3 * sizeof(unsigned int), tris, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glEnableVertexAttribArray(0);

	glBindVertexArray(0);

	additionalInit();
}

void Object::additionalInit()
{
}

void Object::preditionalDisplay()
{
}

void Object::display() {
	glUseProgram(basicShader.handle());  // use the shader

	preditionalDisplay();

	if (invisible)
		return;
	//increment the spin variable per frame.

	ModelViewMatrix = glm::translate(viewMatrix, position);	//simple translate
	ModelViewMatrix = glm::scale(ModelViewMatrix, scale);
	ModelViewMatrix = glm::rotate(ModelViewMatrix, rotation.x, glm::vec3(1, 0, 0)); //rotation about x axis
	ModelViewMatrix = glm::rotate(ModelViewMatrix, rotation.y, glm::vec3(0, 1, 0)); //rotation about y axis
	ModelViewMatrix = glm::rotate(ModelViewMatrix, rotation.z, glm::vec3(0, 0, 1)); //rotation about z axis

	glUniformMatrix4fv(glGetUniformLocation(basicShader.handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);

	//draw objects
	glBindVertexArray(m_vaoID);		// select VAO

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glDrawElements(GL_TRIANGLES, numOfTris * 3, GL_UNSIGNED_INT, 0);

	// Done

	additionalDisplay();
}

void Object::additionalDisplay()
{
}

void Object::update() {

}

void Object::keyboard() {
	
}

Object::~Object()
{
}

ModelObject::ModelObject(char* modelName)
{
	this->modelName = modelName;
}

void ModelObject::createGeometry() {
	glUseProgram(textureShader.handle());  // use the shader

	glEnable(GL_TEXTURE_2D);

	cout << " loading model " << endl;
	if (objLoader.loadModel(modelName, model))//returns true if the model is loaded, puts the model in the model parameter
	{
		cout << " model loaded " << endl;

		//if you want to translate the object to the origin of the screen,
		//first calculate the centre of the object, then move all the vertices
		//back so that the centre is on the origin.
		//model.calcCentrePoint();
		//model.centreOnZero();


		model.calcVertNormalsUsingOctree();  //the method will construct the octree if it hasn't already been created.


											 //turn on VBO by setting useVBO to true in threeDmodel.cpp default constructor - only permitted on 8 series cards and higher
		model.initDrawElements();
		model.initVBO(&textureShader);
		model.deleteVertexFaceData();

	}
	else
	{
		cout << " model failed to load " << endl;
	}

	additionalInit();
}

void ModelObject::display() {
	if (invisible)
		return;

	preditionalDisplay();

	glUseProgram(textureShader.handle());  // use the shader

	ModelMatrix = glm::mat4(1.0f);

	ModelMatrix = glm::translate(ModelMatrix, position);	//simple translate

	ModelMatrix = glm::scale(ModelMatrix, scale);	//simple scale

	ModelMatrix = glm::rotate(ModelMatrix, rotation.y, glm::vec3(0, 1, 0)); //rotation about y axis
	ModelMatrix = glm::rotate(ModelMatrix, rotation.x, glm::vec3(1, 0, 0)); //rotation about x axis
	ModelMatrix = glm::rotate(ModelMatrix, rotation.z, glm::vec3(0, 0, 1)); //rotation about z axis

	ModelViewMatrix = viewMatrix * ModelMatrix;

	glUniform4fv(glGetUniformLocation(textureShader.handle(), "material_ambient"), 1, Material_Ambient);
	glUniform4fv(glGetUniformLocation(textureShader.handle(), "material_diffuse"), 1, Material_Diffuse);
	glUniform4fv(glGetUniformLocation(textureShader.handle(), "material_specular"), 1, Material_Specular);
	glUniform1f(glGetUniformLocation(textureShader.handle(), "material_shininess"), Material_Shininess);

	glm::mat3 normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
	glUniformMatrix3fv(glGetUniformLocation(textureShader.handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);

	//Pass the uniform for the modelview matrix - in this case just "r"
	glUniformMatrix4fv(glGetUniformLocation(textureShader.handle(), "ModelMatrix"), 1, GL_FALSE, &ModelMatrix[0][0]);

	glUniformMatrix4fv(glGetUniformLocation(textureShader.handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
	model.drawElementsUsingVBO(&textureShader);

	glFlush();

	additionalDisplay();

	if(boxes)
		model.drawOctreeLeaves(&basicShader);
}

ModelObject::~ModelObject()
{
}