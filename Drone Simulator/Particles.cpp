#include "Particles.h"

Particle::Particle(glm::vec3 position, glm::vec3 rotation, float lifespan) : Object(0.1f, 0.1f, 0.1f)
{
	glm::vec3 shift = glm::vec3(50 - rand() % 100, 10 - rand() % 20, 50 - rand() % 100);
	shift /= 10000;
	this->position = position + shift;

	shift = glm::vec3(rand() % 360, rand() % 360, rand() % 360);
	this->rotation = rotation + shift;

	this->lifespan = lifespan;
	this->maxLifespan = lifespan;

	this->velocity = glm::vec3(-5 + rand() % 10, 25 + rand() % 25, -5 + rand() % 10);
	this->velocity /= 10000.0f;

	//std::cout << "new particle at " << position.x << " " << position.y << " " << position.z << std::endl;
}

void Particle::setColours() {
	float red = 0.4f, green = 0.4f, blue = 0.4f;

	cols[0] = red;   cols[1] = green;  cols[2] = blue;		//0
	cols[3] = red;   cols[4] = green;  cols[5] = blue;		//1
	cols[6] = red;   cols[7] = green;  cols[8] = blue;		//2
	cols[9] = red;   cols[10] = green;  cols[11] = blue;	//3

	cols[12] = red;   cols[13] = green;  cols[14] = blue;	//4
	cols[15] = red;   cols[16] = green;  cols[17] = blue;	//5
	cols[18] = red;   cols[19] = green;  cols[20] = blue;	//6
	cols[21] = red;   cols[22] = green;  cols[23] = blue;	//7
}

void Particle::update() {
	position += velocity * deltaT;

	scale = glm::vec3(1,1,1) * 1.0f + 10.0f*((maxLifespan-lifespan)/maxLifespan);

	//std::cout << ((maxLifespan - lifespan) / maxLifespan) << std::endl;

	lifespan -= 1.0f*deltaT;

	//std::cout << "particle at " << position.x << " " << position.y << " " << position.z << std::endl;
}

Particle::~Particle()
{
	
}



ParticleSystem::ParticleSystem()
{
	maxParticles = 1000;
}

void ParticleSystem::display() {
	for (Particle* p : particles)
		p->display();
}

void ParticleSystem::update(glm::vec3 position, glm::vec3 rotation, bool constant)
{
	this->position = position; this->rotation = rotation;

	//std::cout << "Updating particle system" << std::endl;
	if (constant && particlesEnabled) {
		if (timer <= 0 && particles.size() < maxParticles) {
			Particle* particle = new Particle(position, rotation, 500);
			particle->createGeometry();
			particles.push_back(particle);
			//std::cout << "Particle made" << std::endl;
			timer = 5.0f;
		}
		else
			timer -= 1.0f*deltaT;
	}

	if (burst > 0 && particles.size() < maxParticles) {
		Particle* particle = new Particle(position, rotation, 1000);
		particle->createGeometry();
		particles.push_back(particle);
		burst--;
	}

	std::list<Particle*> tempList = particles; //Gotta be a more optimised way to do this
	for (Particle* p : particles) {
		p->update();
		if (p->lifespan <= 0) {
			tempList.remove(p);
			delete p;
		}
	}
	particles = tempList;

	//std::cout << particles.size() << std::endl;
}

ParticleSystem::~ParticleSystem()
{
}
