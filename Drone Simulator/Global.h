#pragma once

#include <iostream>
#include <windows.h>		// Header File For Windows
#include "shaders/Shader.h"   // include shader header file, this is not part of OpenGL
//Shader.h has been created using common glsl structure

#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "glm\gtc\matrix_inverse.hpp"

//--- OpenGL ---
#include "gl/glew.h"
#include "gl/wglew.h"
#pragma comment(lib, "glew32.lib")
//--------------

//#include <vector>
//#include "Drone.h"

extern float deltaT;
extern Shader basicShader;
extern Shader textureShader;
extern glm::mat4 viewMatrix;
extern bool keys[256];			// Array Used in the keyboard method.

extern int	mouse_x, mouse_y;
extern bool LeftPressed;

extern bool debug;
extern bool boxes;
extern bool particlesEnabled;

extern glm::vec3 playerPos;
//extern std::vector<AIDrone*> enemyDrones;