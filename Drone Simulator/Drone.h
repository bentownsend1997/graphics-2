#pragma once
#include "Object.h"
#include "Particles.h"

class OffLight : public ModelObject
{
public:

	OffLight();
	void preditionalDisplay();
	void additionalDisplay();
	//void update();
	//void display();
	~OffLight();
};

class RedLight : public ModelObject
{
public:

	RedLight();
	void preditionalDisplay();
	void additionalDisplay();
	//void update();
	//void display();
	~RedLight();
};

class GreenLight : public ModelObject
{
public:

	GreenLight();
	void preditionalDisplay();
	void additionalDisplay();
	//void update();
	//void display();
	~GreenLight();
};

class Blade : public ModelObject
{
public:
	glm::vec3 displacement;
	float spin;
	float factor = 1.0f;

	Blade();
	void update();
	void display();
	~Blade();
};

class Drone : public ModelObject
{
public:
	glm::vec3 velocity = glm::vec3(0, 0, 0);
	glm::vec3 acceleration = glm::vec3(0, 0, 0);
	glm::vec3 targetVelocity = glm::vec3(0, 0, 0);
	float rotationalVelocity = 0;
	float rotationalAcceleration = 0;
	float rotationalTargetVelocity = 0;

	glm::vec3 absoluteVelocity = glm::vec3(0, 0, 0);
	
	std::vector<Blade*> blades;
	std::vector<glm::vec3*> testPoints;

	ParticleSystem* particleSystem;

	RedLight* redLight;
	GreenLight* greenLight;
	OffLight* offLight;
	int lightState = 0;
	float lightCoolDown = 0;

	float invulnerable = 0;
	int health = 5;

	Drone();
	void additionalInit();
	void update();
	void preditionalDisplay();
	virtual void keyboard();
	void respawn(glm::vec3 position);
	~Drone();
};

class AIDrone : public Drone {
public:
	AIDrone();
	void keyboard();
	~AIDrone();
};