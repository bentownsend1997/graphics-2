#pragma once

#include "Global.h"

//MODEL LOADING
#include "3DStruct\threeDModel.h"
#include "Obj\OBJLoader.h"

class Line
{
public:
	unsigned int m_vaoID;		    // vertex array object
	unsigned int m_vboID[2];		// two VBOs - used for colours and vertex data

	const int numOfVerts = 2;
	float verts[6];
	float cols[6];

	glm::vec3 point1 = glm::vec3(0,0,0);
	glm::vec3 point2 = glm::vec3(0, 0, 0);

	Line();
	void createGeometry();
	void display();
	void update();
	~Line();
};

class Object
{
public:
	const float PI = 3.14159265358f;

	const int numOfVerts = 8;
	const int numOfTris = 12;
	float verts[24];
	float cols[24];
	unsigned int tris[36];

	bool invisible = false;
	bool online = true;

	unsigned int m_vaoID;		    // vertex array object
	unsigned int m_vboID[2];		// two VBOs - used for colours and vertex data
	GLuint ibo;                     //identifier for the triangle indices

	glm::mat4 ModelMatrix;
	glm::mat4 ModelViewMatrix;  // matrix for the modelling and viewing

	glm::vec3 size = glm::vec3(1, 1, 1);
	glm::vec3 position = glm::vec3(0, 0, 0);
	glm::vec3 rotation = glm::vec3(0, 0, 0);
	glm::vec3 scale = glm::vec3(1, 1, 1);

	Object();
	Object(float xPos, float yPos, float zPos);
	virtual void setColours();
	virtual void createGeometry();
	virtual void additionalInit();
	virtual void preditionalDisplay();
	virtual void display();
	virtual void additionalDisplay();
	virtual void update();
	virtual void keyboard();
	~Object();
};

class ModelObject : public Object
{
public:
	char* modelName;

	ThreeDModel model;
	OBJLoader objLoader;

	//Material properties
	float Material_Ambient[4] = { 0.5f, 0.5f, 0.5f, 1.0f };
	float Material_Diffuse[4] = { 0.8f, 0.8f, 0.8f, 1.0f };
	float Material_Specular[4] = { 0.9f,0.9f,0.8f,1.0f };
	float Material_Shininess = 50;

	ModelObject(char* modelName);
	void createGeometry();
	void display();
	~ModelObject();
};

