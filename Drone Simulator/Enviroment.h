#pragma once

#include "Object.h"

class Sky : public Object
{
public:
	Sky();
	void setColours();
	~Sky();
};

class Ground : public ModelObject
{
public:
	Ground();
	~Ground();
};

class Tree : public ModelObject
{
public:
	Tree();
	~Tree();
};

class NightSky : public ModelObject {
public:
	NightSky();
	void preditionalDisplay();
	void additionalDisplay();
	~NightSky();
};

class Water : public ModelObject
{
public:
	Water();
	~Water();
};

class Cabin : public ModelObject {
public:
	Cabin();
	~Cabin();
};

class Title : public ModelObject {
public:
	Title();
	void preditionalDisplay();
	void additionalDisplay();
	~Title();
};