#include "Enviroment.h"

Sky::Sky() : Object(200.0f, 200.0f, 200.0f) {

}

void Sky::setColours()
{
	cols[0] = 0.01f;   cols[1] = 0.66f;  cols[2] = 0.95f;		//0
	cols[3] = 0.24f;   cols[4] = 0.31f;  cols[5] = 0.70f;		//1
	cols[6] = 0.24f;   cols[7] = 0.31f;  cols[8] = 0.70f;		//2
	cols[9] = 0.01f;   cols[10] = 0.66f;  cols[11] = 0.95f;	//3

	cols[12] = 0.01f;   cols[13] = 0.66f;  cols[14] = 0.95f;	//4
	cols[15] = 0.24f;   cols[16] = 0.31f;  cols[17] = 0.70f;	//5
	cols[18] = 0.24f;   cols[19] = 0.31f;  cols[20] = 0.70f;	//6
	cols[21] = 0.01f;   cols[22] = 0.66f;  cols[23] = 0.95f;	//7
}

Sky::~Sky() {

}

Ground::Ground() : ModelObject("models/ground.obj") {
	//Material_Ambient[0] = 0.1f; Material_Ambient[1] = 0.1f; Material_Ambient[2] = 0.1f;
	//Material_Diffuse[0] = 0.8f; Material_Diffuse[1] = 0.8f; Material_Diffuse[2] = 0.5f;
	Material_Specular[0] = 0.1f; Material_Specular[1] = 0.1f; Material_Specular[2] = 0.1f;
	Material_Shininess = 50;
}

Ground::~Ground() {

}

Tree::Tree() : ModelObject("models/tree.obj") {
	//Material_Ambient[0] = 0.1f; Material_Ambient[1] = 0.1f; Material_Ambient[2] = 0.1f;
	//Material_Diffuse[0] = 0.8f; Material_Diffuse[1] = 0.8f; Material_Diffuse[2] = 0.5f;
	Material_Specular[0] = 0.1f; Material_Specular[1] = 0.1f; Material_Specular[2] = 0.1f;
	Material_Shininess = 50;
}

Tree::~Tree() {

}

NightSky::NightSky() : ModelObject("models/nightSky.obj") {
	Material_Ambient[0] = 1.0f; Material_Ambient[1] = 1.0f; Material_Ambient[2] = 1.0f;
	Material_Diffuse[0] = 1.0f; Material_Diffuse[1] = 1.0f; Material_Diffuse[2] = 1.0f;
	Material_Specular[0] = 1.0f; Material_Specular[1] = 1.0f; Material_Specular[2] = 1.0f;
	Material_Shininess = 50;
	
	scale *= 3.0f;
}

void NightSky::preditionalDisplay() {
	glUniform1i(glGetUniformLocation(textureShader.handle(), "fogEnabled"), false);
}

void NightSky::additionalDisplay() {
	glUniform1i(glGetUniformLocation(textureShader.handle(), "fogEnabled"), true);
}

NightSky::~NightSky() {

}

Water::Water() : ModelObject("models/water.obj") {
	//Material_Ambient[0] = 0.1f; Material_Ambient[1] = 0.1f; Material_Ambient[2] = 0.1f;
	//Material_Diffuse[0] = 0.8f; Material_Diffuse[1] = 0.8f; Material_Diffuse[2] = 0.5f;
	Material_Specular[0] = 0.5f; Material_Specular[1] = 0.5f; Material_Specular[2] = 0.5f;
	Material_Shininess = 100;
}

Water::~Water() {

}

Cabin::Cabin() : ModelObject("models/cabin.obj") 
{
}

Cabin::~Cabin()
{
}

Title::Title() : ModelObject("models/title.obj") {
	Material_Ambient[0] = 1.0f; Material_Ambient[1] = 1.0f; Material_Ambient[2] = 1.0f;
	Material_Diffuse[0] = 1.0f; Material_Diffuse[1] = 1.0f; Material_Diffuse[2] = 1.0f;
	Material_Specular[0] = 1.0f; Material_Specular[1] = 1.0f; Material_Specular[2] = 1.0f;
	Material_Shininess = 50;

	scale *= 3.0f;
}

void Title::preditionalDisplay() {
	glUniform1i(glGetUniformLocation(textureShader.handle(), "fogEnabled"), false);
}

void Title::additionalDisplay() {
	glUniform1i(glGetUniformLocation(textureShader.handle(), "fogEnabled"), true);
}

Title::~Title() {

}
